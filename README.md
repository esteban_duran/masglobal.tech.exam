## Backend
In order to run the application go to the terminal and work your way to the folder containing the source code. Once you're there, run the following command:

```sh
$ mvn spring-boot:run
```

The application backend should now be up and running in `localhost:8989`. [Take me to Swagger!](http://localhost:8989/masglobal.tech.exam/swagger-ui.html#/)


## Frontend
To run the webapp, do as follows: 
1) Go to source code folder
2) run this command:
```sh
$ start index.html
```

Your browser should be opened with the webapp loaded. Please remember: BACKEND MUST BE RUNNING!