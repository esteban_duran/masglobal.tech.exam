package masglobal.tech.exam.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import masglobal.tech.exam.facade.impl.MasGlobalTestAPIFacade;
import masglobal.tech.exam.facade.model.Employee;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.service.impl.EmployeeService;
import masglobal.tech.exam.service.impl.HourlyEmployeeFactory;

@ActiveProfiles("default")
@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

	@InjectMocks
	private EmployeeService employeeService;
	
	@Mock
	private MasGlobalTestAPIFacade facade;
	
	@Mock
	private HourlyEmployeeFactory hourlyFactory;
	
	private Employee hourlyEmployee;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		hourlyEmployee = new Employee();
		hourlyEmployee.setId(1);
		hourlyEmployee.setContractTypeName("HourlySalaryEmployee");
	    hourlyEmployee.setHourlySalary(new BigDecimal("35000"));
	    hourlyEmployee.setMonthlySalary(new BigDecimal("450000"));
	    hourlyEmployee.setName("Mark");
	    hourlyEmployee.setRoleId(1);
	    hourlyEmployee.setRoleName("Role Name");
	    hourlyEmployee.setRoleDescription("Role Description");
	}

	@Test
	public void getAllEmployeesWithAnnualSalaryTest() {
		List<Employee> employees = new ArrayList<>();
	    employees.add(hourlyEmployee);
	    
	    Mockito.when(hourlyFactory.createEmployee(hourlyEmployee)).thenCallRealMethod();
		Mockito.when(facade.getEmployees()).thenReturn(employees);
		List<EmployeeDTO> employeesDtosList = employeeService.getEmployeesWithAnnualSalary();
		assertNotNull(employeesDtosList);
		assertEquals(1, employeesDtosList.size());
		
		
		EmployeeDTO dto = employeesDtosList.get(0);
		assertEquals(hourlyEmployee.getId(), dto.getId());
		assertEquals(hourlyEmployee.getContractTypeName(), dto.getContractTypeName());
		assertEquals(hourlyEmployee.getName(), dto.getName());
		assertEquals(hourlyEmployee.getRoleId(), dto.getRoleId());
		assertEquals(hourlyEmployee.getRoleName(), dto.getRoleName());
		assertEquals(hourlyEmployee.getRoleDescription(), dto.getRoleDescription());
		assertEquals(hourlyEmployee.getHourlySalary(), dto.getHourlySalary());
		assertEquals(hourlyEmployee.getMonthlySalary(), dto.getMonthlySalary());
		assertEquals(0, new BigDecimal("50400000").compareTo(dto.getAnnualSalary()));
	}

	@Test
	public void getAllEmployeesWithAnnualSalaryTest_empty() {
		Mockito.when(facade.getEmployees()).thenReturn(Collections.EMPTY_LIST);
		List<EmployeeDTO> employeesDtosList = employeeService.getEmployeesWithAnnualSalary();
		assertNotNull(employeesDtosList);
		assertEquals(0, employeesDtosList.size());
	}
}
