package masglobal.tech.exam.facade;

import java.util.List;

import masglobal.tech.exam.facade.model.Employee;

public interface IMasGlobalTestAPIFacade {

	List<Employee> getEmployees();
}
