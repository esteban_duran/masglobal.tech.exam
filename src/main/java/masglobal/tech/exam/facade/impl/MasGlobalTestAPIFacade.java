package masglobal.tech.exam.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import lombok.Data;
import masglobal.tech.exam.facade.IMasGlobalTestAPIFacade;
import masglobal.tech.exam.facade.model.Employee;

@Component
@Data
public class MasGlobalTestAPIFacade implements IMasGlobalTestAPIFacade {

	@Value("${masglobal.api.baseUrl}")
	private String masglobalURL;

	@Value("${masglobal.api.employees}")
	private String employeesURL;

	private RestTemplate restTemplate;

	@Autowired
	public MasGlobalTestAPIFacade(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public List<Employee> getEmployees() {
		return restTemplate.exchange(this.buildEmployeesURL(), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Employee>>() {
				}).getBody();
	}

	private String buildEmployeesURL() {
		return masglobalURL.concat(employeesURL);
	}
}
