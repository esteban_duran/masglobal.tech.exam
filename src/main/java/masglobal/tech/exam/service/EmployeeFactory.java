package masglobal.tech.exam.service;

import masglobal.tech.exam.facade.model.Employee;

public abstract interface EmployeeFactory {

	EmployeeDTO createEmployee(Employee employee);
}
