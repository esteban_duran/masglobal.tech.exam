package masglobal.tech.exam.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import masglobal.tech.exam.facade.IMasGlobalTestAPIFacade;
import masglobal.tech.exam.facade.model.Employee;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.service.EmployeeFactory;
import masglobal.tech.exam.service.IEmployeeService;
import masglobal.tech.exam.utils.Constants;

@Service
@NoArgsConstructor
public class EmployeeService implements IEmployeeService {

	private IMasGlobalTestAPIFacade masglobalFacade;
	private EmployeeFactory hourlyEmployeeFactory;
	private EmployeeFactory monthlyEmployeeFactory;

	@Autowired
	public EmployeeService(IMasGlobalTestAPIFacade masglobalFacade,
			@Qualifier("hourlyEmployeeFactory") EmployeeFactory hourlyEmployeeFactory,
			@Qualifier("monthlyEmployeeFactory") EmployeeFactory monthlyEmployeeFactory) {
		this.masglobalFacade = masglobalFacade;
		this.hourlyEmployeeFactory = hourlyEmployeeFactory;
		this.monthlyEmployeeFactory = monthlyEmployeeFactory;
	}

	@Override
	public List<EmployeeDTO> getEmployeesWithAnnualSalary() {
		List<EmployeeDTO> employeesList = new ArrayList<>();
		masglobalFacade.getEmployees().forEach(emp -> {
			EmployeeDTO dto = this.createEmployeeDTO(emp);
			dto.calculateAnnualSalary();
			employeesList.add(dto);
		});
		return employeesList;
	}

	@Override
	public List<EmployeeDTO> getEmployeesWithAnnualSalary(List<Integer> employeeIds) {
		return getEmployeesWithAnnualSalary().stream().filter(emp -> employeeIds.contains(emp.getId()))
				.collect(Collectors.toList());
	}

	private EmployeeDTO createEmployeeDTO(Employee employee) {
		EmployeeDTO employeeDTO;
		if (Constants.SALARY_TYPE_HOURLY.equals(employee.getContractTypeName())) {
			employeeDTO = hourlyEmployeeFactory.createEmployee(employee);
		} else {
			employeeDTO = monthlyEmployeeFactory.createEmployee(employee);
		}
		return employeeDTO;
	}

}
