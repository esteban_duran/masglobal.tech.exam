package masglobal.tech.exam.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import masglobal.tech.exam.facade.model.Employee;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.service.EmployeeFactory;

@Component
@Qualifier("monthlyEmployeeFactory")
public class MonthlyEmployeeFactory implements EmployeeFactory {

	@Override
	public EmployeeDTO createEmployee(Employee employee) {
		return new MonthlyEmployee(employee);
	}

}
