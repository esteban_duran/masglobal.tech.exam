package masglobal.tech.exam.service.impl;

import masglobal.tech.exam.facade.model.Employee;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.utils.Constants;

public class HourlyEmployee extends EmployeeDTO{
	
	public HourlyEmployee(Employee employee) {
		super(employee);
		setContractTypeName(Constants.SALARY_TYPE_HOURLY);
	}

	@Override
	public void calculateAnnualSalary() {
		setAnnualSalary(Constants.HOUR_FACTOR.multiply(getHourlySalary()).multiply(Constants.YEAR_MONTHS));
	}

}
