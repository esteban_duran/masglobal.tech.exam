package masglobal.tech.exam.service.impl;

import masglobal.tech.exam.facade.model.Employee;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.utils.Constants;

public class MonthlyEmployee extends EmployeeDTO {

	public MonthlyEmployee(Employee employee) {
		super(employee);
		setContractTypeName(Constants.SALARY_TYPE_MONTHLY);
	}

	@Override
	public void calculateAnnualSalary() {
		setAnnualSalary(Constants.YEAR_MONTHS.multiply(getMonthlySalary()));
	}

}
