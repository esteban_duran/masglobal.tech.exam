package masglobal.tech.exam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import masglobal.tech.exam.service.EmployeeDTO;
import masglobal.tech.exam.service.IEmployeeService;
import masglobal.tech.exam.utils.Constants;

@CrossOrigin
@RestController
@RequestMapping("/employees")
public class EmployeeController {

	private IEmployeeService employeeService;

	@Autowired
	public EmployeeController(IEmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@GetMapping("/annualSalaries/all")
	@ApiOperation(value = "Gets all employees with annual salaries info.", tags = { Constants.EMPLOYEE_TAG })
	public List<EmployeeDTO> employeesWithAnnualSalaries() {
		return employeeService.getEmployeesWithAnnualSalary();
	}

	@GetMapping("/annualSalaries/id={employeeId}")
	@ApiOperation(value = "Gets employees by ID with annual salaries info.", tags = { Constants.EMPLOYEE_TAG })
	@ApiImplicitParams(
			@ApiImplicitParam(required = true, value = "Employees id", name = "employeeId", paramType = "path", allowMultiple = true))
	public List<EmployeeDTO> employeesByIdWithAnnualSalaries(@PathVariable List<Integer> employeeId) {
		return employeeService.getEmployeesWithAnnualSalary(employeeId);
	}
}
