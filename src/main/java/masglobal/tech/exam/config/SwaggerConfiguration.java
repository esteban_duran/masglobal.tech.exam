package masglobal.tech.exam.config;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfiguration {

	@Value("${project.basePackage}")
	private String basePackage;

	/**
	 * Swagger config.
	 */
	@Bean
	public Docket swagger() {
		return new Docket(DocumentationType.SWAGGER_2).directModelSubstitute(LocalDate.class, java.sql.Date.class)
				.directModelSubstitute(LocalTime.class, String.class).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage(basePackage)).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("masglobal.tech.exam").description("MasGlobal Tech Exam").version("0.0.1")
				.build();
	}

}